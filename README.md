<!--- vim: set et sw=2 ts=2 tw=80 : -->

![DrBrainf-ck](./imgs/logo_small.png)

# DrBrainf-ck

This is the repo for the USI [PF1](https://usi-pl.github.io/pf1-2018/)
final project of _Claudio Maggioni_ and _Tommaso Rodolfo Masera_.

## Running

Run the file `gui.rkt` using *DrRacket* or the `racket` CLI tool.

A CLI version of the interpreter is avaliable. In order to run it, execute:

`./cli.rkt <name-of-file.bf>`

More documentation of this command is avaliable using the `-h` flag.

## Current features

- Brainf-ck interpreter works for all instructions;
- Done simple editor GUI with *Run* button and output window;
- Editor supports basic syntax highlighting;
- Batch input through `,` supported through GUI.

## Documentation

Please find more documentation in the file `README.pdf`.

## Changes from proposal

Please find the document regarding changes from the original proposal in the
file `changes.pdf`.

## License

This project is licensed under the MIT license.
